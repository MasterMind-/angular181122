import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LogDirectiveDirective } from './directives/log-directive.directive';
import { StyleDirectiveDirective } from './directives/style-directive.directive';
import { FiledropDirective } from './directives/filedrop.directive';

@NgModule({
  declarations: [
    AppComponent,
    LogDirectiveDirective,
    StyleDirectiveDirective,
    FiledropDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
