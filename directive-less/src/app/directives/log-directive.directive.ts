import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appLogDirective]'
})
export class LogDirectiveDirective {

  @HostListener('input') onInput($event: KeyboardEvent ) {
    console.log((<HTMLInputElement>event.target).value);
  }

  constructor() { }

}
