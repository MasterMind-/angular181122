import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('filesInput') filesInput: ElementRef;
  @ViewChild('photo') photo: ElementRef;
  files = [];

  onOpenFilesInput() {
    this.filesInput.nativeElement.click();
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
  }

  onFilesChangeInput(event) {
    for (let i = 0; i < event.srcElement.files.length; i++) {
      this.files.push(event.srcElement.files[i]);
    }
    console.log(this.files);
  }

  onFileDelete(idx) {
    this.files.splice(idx, 1);
  }
}
