import { Directive, Renderer, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appStyleDirective]'
})
export class StyleDirectiveDirective {

  constructor(renderer: Renderer2, el: ElementRef) {
    renderer.setStyle(el.nativeElement, 'background-color', 'green');
  }

}
